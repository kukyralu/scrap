import requests
from bs4 import BeautifulSoup, BeautifulStoneSoup
from requests.models import HTTPError

def getTitle(url):
    try:
        res = requests.get(url)
    except HTTPError as e:
        return None
    try:
        bs = BeautifulSoup(res.text, 'html.parser')
        titulo = bs.h1
    except AttributeError as e:
        return None
    return titulo


if __name__ == '__main__':
    
    print(getTitle('http://www.mundonovacasa.com'))